<?php $base = "/LifeCycle/wp-content/themes/LifeCycle/";?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>LIFECYCLE Marketing</title>
    <link rel="stylesheet" href="<?php echo $base;?>style.css" />
    <script src="<?php echo $base;?>js/vendor/modernizr.js"></script>
    <script src="<?php echo $base;?>js/foundation/foundation.topbar.js"></script>
</head>
<body>
<div class="contain-to-grid sticky">

<nav class="top-bar show-for-small-only" data-topbar> 
    <ul class="title-area"> 
        <li class="name"> 
            <h1><a href="#">LifeCycle</a></h1> </li>
        <li class="toggle-topbar icon"><a href="#"><span>Menu</span></a></li> 
    </ul>    
    <section class="top-bar-section"> <!-- Right Nav Section --> 
        <ul class="right"> 
            <li class="active"><a href="#">Home</a></li>
               <li><a href="#qualify">Qualify</a></li>
               <li><a href="#why">Why</a></li>
               <li><a href="#ourprocess">Our Process</a></li>
               <li><a href="#whatwedo">What We Do</a></li>
               <li><a href="#results">Results</a></li>
               <li><a href="#pricing">Pricing</a></li>
        </ul> <!-- Left Nav Section --> 
    </section> 
</nav>    
<nav class="menu show-for-medium-up">
    <div class="">
<div class="row">
 <div class="left">
  <h1 class="logo"><a href=""><img src="<?php echo $base;?>/img/logo.png" /></a></h1>
 </div>
 <div class="right">
  <ul class="topLinks hide-for-medium-only topmenu">
               <li><a href="#qualify">Qualify</a></li>
               <li><a href="#why">Why</a></li>
               <li><a href="#ourprocess">Our Process</a></li>
               <li><a href="#whatwedo">What We Do</a></li>
               <li><a href="#results">Results</a></li>
               <li><a href="#pricing">Pricing</a></li>
   <li><a href="#" class="phone">(480) 442 - 6556</a></li>      
   <li><a href="#" class="button expand">Start My Application Process</a></li>
  </ul>
  <ul class="topLinks show-for-medium-only topmenu">
               <li><a href="#qualify">Qualify</a></li>
               <li><a href="#why">Why</a></li>
               <li><a href="#ourprocess">Our Process</a></li>
               <li><a href="#whatwedo">What We Do</a></li>
               <li><a href="#results">Results</a></li>
               <li><a href="#pricing">Pricing</a></li>
   <li><a href="#" class="phone">(480) 442 - 6556</a></li>      
   <li><a href="#" class="button expand">Start My Application Process</a></li>
  </ul>
 </div>
 </div>
    </div>
 </nav>
    </div>