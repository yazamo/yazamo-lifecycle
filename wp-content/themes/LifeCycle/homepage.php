<?php $base = "/LifeCycle/wp-content/themes/LifeCycle/";?>
<?php
/**
 * Template Name: Home Template
 */

get_header(); ?>


<section class="qualify" id="qualify">
    
    <div class="row cushion">
        <div class="large-10 medium-10 small-10 columns">
            <div class="row">
                <div class="large-7 medium-7 columns videosection">
                    <img src="<?php echo $base; ?>img/Video.png" />
                </div>
                <div class="large-5 medium-5 columns qualifyform text-center">
                    <form>
                    <fieldset class="qualifyformbg">
                      <div class-"row">
                          <p class="qualifyformtitle avenirbold sitecolor">Are You Ready To Double Your Sales Online?</p>
                          <p class="qualifyformtext">In order to deliver phenomenal results, we can only work with a couple new clients each month. To work with us and begin receiving proven marketing strategy and implementation, please enter your first name and email below to start the application process.</p>
                      </div>       
                      <div class="row">
                        <div class="large-12 columns">
                            <input type="text" placeholder="Enter your name" />
                        </div>
                        <div class="large-12 columns">
                            <input type="text" placeholder="Enter your email address" />
                        </div>
                        <div class="large-12 columns">
                            <input type="submit" class="button radius appsubmitbutton" value="Start My Application Process" />
                        </div>                            
                      </div>
                    </fieldset>    
                    </form>
                </div>
            </div>
            
        </div>
    </div>   

</section>




<section class="why" id="why">
    
    <div class="row cushion">
        <div class="large-10 medium-10 small-10 columns">
            
            <div class="row"> <!-- sectiontitle -->
                <div class="large-5 medium-5 small-5 columns large-centered medium-centered small-centered sectiontitle">
                    <div class="text-center">
                    <span class="title">why</span>
                    </div>
                </div>
            </div>
            
            <div class="row">
                <div class="columns text-center">
                    <p class="introtext">So why us?  Simply put our approach is holistic, we understand building a business isn’t just about getting more leads.  Yes we want to generate more leads and sales, but we want to help you build lifetime customers, not just customers who buy once and leave.  We help our clients acquire customers who buy from you again and again and refer you even more business after they buy.</p>
                    <p class="introtext">Most of our clients are like Danny from the video, they have had success in traditional marketing but are having trouble transitioning into the digital world.  </p>
                </div>
            
            </div>
            
            <div class="row">
                <div class="large-6 columns text-left" style="background: #ececec;">
                    <ol>
                        <li>Hiring a firm to create a big expensive website with little or no marketing strategy built in.  They spend a lot money and don’t see leads and sales increase.</li>
                        <li>Spending money to acquire clients but quickly losing them. They have no automated ability to stay in contact with a client to keep them engaged and buying.
</li>                        
                        <li>Using ideas from other entrepreneurs and trying 
to quickly and sporadically implement them in their business seeing minimal results.
</li>                    
                        <li>Creating digital content that is not engaging or valuable to potential clients. 
</li>                    
                        <li>Misunderstanding just how much money every 
single client is worth.  Phrased differently, what is 
the life time value of their clients.</li>                    
                    </ol>
                
                
                
                </div>
                <div class="large-6 columns text-right">
                    <p>If you’ve made any of these mistakes, you are not alone.  We have developed the Yazamo Lifecycle Marketing Process for businesses like yours, investing hundreds of thousands of dollars through years of testing, tracking and working on some very big marketing campaigns resulting in millions in sales so that you can avoid these exact mistakes.</p>
                    <p>
                    We’ve worked with celebrity clients like Arianna Huffington, founder of the media giant, The Huffington Post; Peter Diamandis, founder of the $10 million dollar XPrize, which jump started commercial space travel; and Joe Polish, founder of the Genius Network and one of the smartest connectors and marketing minds on the planet. 
                    </p>
                </div>
            </div>
            
            <div class="row">
                <div class="columns text-right">
                    <p>Just to be completely clear our 
approach is NOT a quick fix. </p>
                    <p>We don’t have a crystal ball into your business that tells us exactly how your customers will respond. This means it may take a couple months for us to learn and adapt before you see a return on investment.  What we do know is the process and the strategies used have produced results for many of our clients, often 
doubling their investment after two months.</p>
                    </div>
            </div>
            
            <div class="row">
                <div class="columns text-center">
                    <p><img src="<?php echo $base; ?>img/TwoIcons.png" /></p>
                </div>
            </div>            
            
             <div class="row">
                <div class="columns text-center">
                    <p>The Yazamo LifeCycle Marketing Process is designed to increase leads and sales quickly, but it is also designed to scale and multiply the lifetime value of each customer. Our approach systematically targets the areas of your marketing that will produce the quickest results while gradually optimizing your strategy to build lifetime value.  Our LifeCycle Marketing Process is designed specifically to dig deep and identify how to convert your leads into sales, how to convert those sales into lifetime customers, and how to turn those lifetime customers into raving fans who refer you business.</p>
                    <p>To find out if you qualify for our program fill out the form above and discover how the Yazamo LifeCycle Marketing Process can help you build long lasting clients and help grow your business for years to come.</p>
                </div>
            </div>              
            
             <div class="row">
                <div class="columns text-center">
                    <p><img src="<?php echo $base; ?>img/QuoteLogo.png" /></p>
                    <p>What Joe Polish Thinks About Yazamo</p>
                    <p>[JOE POLISH VIDEO]</p>
                </div>
            </div>               
            
        </div>
    </div>          

</section>




<section class="process" id="ourprocess">
    <div class="row cushion">
        <div class="large-10 medium-10 small-10 columns">
            
            <div class="row"> <!-- sectiontitle -->
                <div class="large-5 medium-5 small-5 columns large-centered medium-centered small-centered sectiontitle">
                    <div class="text-center">
                    <span class="title">our process</span>
                    </div>
                </div>
            </div>

             <div class="row">
                <div class="columns text-center">
                    <p>Every client we work with starts with an in-person consultation.</p> 
                    <p class="introtext">No this is not a casual meeting over coffee to chat about your marketing struggles and business goals. The consultation is a deliberate joint effort between us and your team beginning weeks before we ever meet.</p>
                </div>
            </div>      

            
             <div class="row">
                <div class="large-6 medium-6 small-6 columns">
                    <div class="small-3 columns">LOGO</div>
                    <div class="small-9 columncons">Prior to your consultation we install sophisticated tracking software. We analyze and discover what’s working in your marketing strategy and where money is slipping through the cracks.</div>
                </div>
                <div class="large-6 medium-6 small-6 columns">
                    <div class="small-3 columns">LOGO</div>
                    <div class="small-9 columns">Prior to your consultation we install sophisticated tracking software. We analyze and discover what’s working in your marketing strategy and where money is slipping through the cracks.</div>
                </div>                 
             </div>
             <div class="row">
                <div class="large-6 medium-6 small-6 columns">
                    <div class="small-3 columns">LOGO</div>
                    <div class="small-9 columns">Prior to your consultation we install sophisticated tracking software. We analyze and discover what’s working in your marketing strategy and where money is slipping through the cracks.</div>
                </div>
                <div class="large-6 medium-6 small-6 columns">
                    <div class="small-3 columns">LOGO</div>
                    <div class="small-9 columns">Prior to your consultation we install sophisticated tracking software. We analyze and discover what’s working in your marketing strategy and where money is slipping through the cracks.</div>
                </div>                 
             </div>             
            
             <div class="row">
                <div class="columns text-center">
                    <p>What to expect from your cnosultation</p>
                    <p>Meeting Agenda:</p>                    
                </div>
            </div>
            
            <div class="row" style="background: #ececec;">
                <div class="large-6 columns text-left">
                    <ol>
                        <li><a href="javascript:void(0);" id="myLink">You Introduce Us to Your Team</a></li>

                        <li><a href="javascript:void(0);" id="myLink">Group Introductions</a></li>

                        <li><a href="javascript:void(0);" id="myLink">Expectations and Goals</a></li>

                        <li><a href="javascript:void(0);" id="myLink">Openly Discuss Problems and Challenges in Your Business</a></li>

                        <li><a href="javascript:void(0);" id="myLink">Walk Through the Workbook With the Team</a></li>

                        <li><a href="javascript:void(0);" id="myLink">Software Results</a></li>

                        <li><a href="javascript:void(0);" id="myLink">6 Month Plan of Action</a></li>

                        <li><a href="javascript:void(0);" id="myLink">Recap</a></li>

                        <li><a href="javascript:void(0);" id="myLink">Complete Wrap Up and Breakdown Sent Immediately After the Consultation</a></li>                 
                    </ol>
                
                
                </div>
                <div class="large-6 columns text-center" id="agendaloader">
                        <p>7</p>
                        <p>6 Month Plan of Action</p>
                        <p>Now that we have been through the discovery stage we present our initial plan of action. Our goal is to work with you in partnership for the next 10 years and in order to accomplish that we need to provide value from day one, this means getting more leads or sales into your business as quickly as possible.</p>
                        <p>Based on our findings from the consultation process we will create a 6 month plan of action to implement the most effective and efficient solution first in order to impact the bottom line as quickly as possible. We begin with a 6 month plan broken out into 1 month stages.  Each stage is designed to get you results as quickly as possible and build upon the previous month.</p>
                </div>
            </div>            
            
             <div class="row">
                <div class="columns text-center">
                    <p><img src="<?php echo $base; ?>img/QuoteLogo.png" /></p>
                </div>
                <div class="columns text-center">
                    <p>“I just heard the CEO of this company present at an internet marketing event in LA. Yazamo is working with Huffington Post, Xprize and dozens of brilliant people. In the dinner I had with the CEO he give me such incredible advice that my conversion rates have double. I'm not even working with them, but will be. They know what they are talking about.” <br/>- Dr. Isaac Jones</p>
                </div>                 
                 
            </div>            
            
            
        </div>
        
    </div>
</section>





<section class="whatwedo" id="whatwedo">

    <div class="row cushion">
        <div class="large-10 medium-10 small-10 columns">
            
            <div class="row"> <!-- sectiontitle -->
                <div class="large-5 medium-5 small-5 columns large-centered medium-centered small-centered sectiontitle">
                    <div class="text-center">
                    <span class="title">what we do</span>
                    </div>
                </div>
            </div>
            
            <div class="row">
                <div class="columns text-center">
                    <p class="introtext"><strong>This is all the stuff we do FOR you!</strong>  We are NOT just a coach or consultant giving you strategy who reverse delegates the work to you.  We are a full service marketing team. Using the Yazamo LifeCycle Marketing Process we create and implement your marketing strategy while continually monitoring and analyzing your marketing campaigns making adjustments to optimize your results.</p>
                </div>
            </div>            
            
        </div>
    </div>          

</section>

<section class="stuff">




</section>


<section class="features" id="features">

    <div class="row cushion">
        <div class="large-10 medium-10 small-10 columns">
            

            <div class="row" data-equalizer="">
                  <div class="medium-4 columns">
                    <div class="panel" data-equalizer-watch="" style="height: 232px; background: #fff;">
                      <h3>Panel 1</h3>
                      <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante.</p>
                    </div>
                  </div>
                  <div class="medium-4 columns">
                    <div class="callout panel" data-equalizer-watch="" style="height: 232px; background: #fff;">
                      <h3>Panel 2</h3>
                      <ul>
                         <li>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.</li>
                         <li>Aliquam tincidunt mauris eu risus.</li>
                         <li>Vestibulum auctor dapibus neque.</li>
                      </ul>
                    </div>
                  </div>
                  <div class="medium-4 columns">
                    <div class="panel" data-equalizer-watch="" style="height: 232px; background: #fff;">
                      <h3>Panel 3</h3>
                      <p>Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae, ultricies eget, tempor sit amet, ante. Donec eu libero sit amet quam egestas semper. Aenean ultricies mi vitae est. Mauris placerat eleifend leo.</p>
                    </div>
                  </div>
            </div>
        
             <div class="row">
                <div class="columns text-center">
                    <p><img src="<?php echo $base; ?>img/QuoteLogo.png" /></p>
                </div>
                <div class="columns text-center">
                    <p>"I've been on Joe Polish's Piranha Marketing / Genius Network Team for 10 Years. The Yazamo team has been the best students of direct response marketing and cutting edge technology I've seen in the last decade. Their customer support, campaign design, and elegant marketing ideas have created killer results for Piranha / Genius Network and many of our Genius Network Members as well! Call them - you won't regret it.” <br/> - Gina DeLong</p>
                </div>
            </div>
            
            <div class="row">
                <div class="large-5 columns large-centered text-center">
                            <input type="submit" class="button radius appsubmitbuttonbig" value="Start My Application Process" />
                </div> 
            </div>
            
            
        </div>
    </div>          

</section>




<section class="results" id="results">

    <div class="row cushion">
        <div class="large-10 medium-10 small-10 columns">
            
            <div class="row"> <!-- sectiontitle -->
                <div class="large-5 medium-5 small-5 columns large-centered medium-centered small-centered sectiontitle">
                    <div class="text-center">
                    <span class="title">results</span>
                    </div>
                </div>
            </div>
            
            <div class="row">
                
                <div class="large-6 medium-6  columns">
                    <div class="row"><img src="<?php echo $base; ?>img/Result1.png" /></div>
                    <div class="row text-center"><a href="" class="button radius casestudybutton">read full case study here</a></div>
                </div>
                
                <div class="large-6 medium-6 columns text-right">
                    <div class="row"><p>Peter Diamandis and Abundance</p></div>
                    <div class="row"><p>Peter Diamandis is the founder of the $10 million dollar XPrize and is personally responsible for creating the commercial space travel industry. When Peter wrote the NY Times Best Selling book, Abundance, he needed a sales funnel that could monetize his book and perpetuate the marketing of the message of Abundance.</p>
                        <p>We created a sales funnel that gave away his book Abundance, sold an audio course called Exponential Thinking, a conference recording called Exponential and Abundance Thinking, and tickets to the Singularity University Executive Program. We used affiliates to create traffic and email marketing to send people through the funnel.</p>
                    </div>
                    <div class="row campaignresults" style="background: #06596b;">
                        <p>Results of this campaign:</p>
                        <ul>
                            <li>Grew Peter’s email list by 46,000 people</li>    
                            <li>Sold over 18,350 books</li>    
                            <li>Total sales of $430,000</li>
                        </ul>
                    </div>
                </div>
                
            </div>
            
            
            
            <div class="row">
                
                <div class="large-7 columns text-left">
                    <div class="row"><p>GIOVANNI MARSICO and ARCHANGEL ACADEMY</p></div>
                    <div class="row"><p>Giovanni Marsico had a dream that Gifted Entrepreneur will change the world. He says that big-hearted, generous entrepreneurs are going to be the chief change-makers, so he started a business called Archangel Academy in order to coach, connect, and foster collaboration between these entrepreneurs to help make this dream a reality.</p> 

                                    <p>One of Archangel Academy’s trademarks is a series of live events where interested entrepreneurs apply and become part of a hand selected group.</p>

                                    <p>Giovanni needed a predictable way to create leads and sell new audiences into his live events.</p> 

                                    <p>We created and tested different Facebook ads driving people to the event website. From the site, they were put through a qualification process and those who met the criteria were contacted and sold into the event.</p>
                    </div>
                    <div class="row campaignresults" style="background: #06596b;">
                        <p>Results of this campaign:</p>
                        <ul>
                            <li>Received over 420 applications to attend</li>
                            <li>Filled 80% of the conference through Facebook ads at $2,000 per person</li>
                            <li>Total sales of $160,000</li>
                        </ul>
                    </div>
                </div>
                
                <div class="large-5 columns">
                    <div class="row"><img src="<?php echo $base; ?>img/Result2.png" /></div>
                    <div class="row text-center"><a href="" class="button radius casestudybutton">read full case study here</a></div>
                </div>                
                
            </div>
            
            
            <div class="row">
                
                <div class="large-6 columns">
                    <div class="row"><img src="<?php echo $base; ?>img/Result3.png" /></div>
                    <div class="row text-center"><a href="" class="button radius casestudybutton">read full case study here</a></div>
                </div>
                
                <div class="large-6 columns text-right">
                    <div class="row"><p>ZHEALTH</p></div>
                    <div class="row"><p>Zhealth has many different certification classes for personal trainers and teaches those trainers how to reduce pain and increase performance in athletes. They’ve helped over 70,000 people feel and perform better and constantly work with athletes in the NBA, FIFA, NFL, MLB, Olympics, and many other professional organizations.</p>
                                    <p>Zhealth came to us needing a way to capture more leads and sell them into the first certification class, Essentials. This was important because once customers go through the essentials class, they end up paying for most of the other classes Zhealth has to offer because they get such phenomenal results and for that reason they become lifetime customers and raving fans.</p>
                                    <p>We developed a webinar campaign to turn leads that had never bought anything into paying customers. We wanted to get these leads to make their first purchase with Zhealth so that they would know, like, and trust Zhealth, learn about the first Essentials class and then sign up for that Essentials class.</p>
                                    <p>We developed a campaign that was promoted on their website and through the leads on their email list and got them to pay for the webinar. We set up a follow up system and the marketing so we got incredible results of people actually showing up to the webinar. Then we gave them the option to request a phone call from a sales person to talk about next steps.</p>
                    </div>
                    <div class="row campaignresults" style="background: #06596b;">
                        <p>Results of this campaign:</p>
                        <ul>
                            <li>Grew Peter’s email list by 46,000 people</li>    
                            <li>Sold over 18,350 books</li>    
                            <li>Total sales of $430,000</li>
                        </ul>
                    </div>
                </div>
                
            </div>            
          
            
    
            
        </div>
    </div>          

</section>



<section class="pricing" id="pricing">

    <div class="row cushion">
        <div class="large-10 medium-10 small-10 columns">
            
            <div class="row"> <!-- sectiontitle -->
                <div class="large-5 medium-5 small-5 columns large-centered medium-centered small-centered sectiontitle">
                    <div class="text-center">
                    <span class="title">pricing</span>
                    </div>
                </div>
            </div>
            
            
            
             <div class="row">
                <div class="columns text-center">
                    <p>Consultation</p> 
                    <p class="introtext">A $5,000 in-person consultation is required up front. This a meeting between us and your executive team. 
This includes travel expenses to your location in the United States. </p>
                </div>
            </div>
            
            
            
            
             <div class="row">
                <div class="columns text-center">
                    <p>Retainers</p>
                    <p>Every client is required to sign a 6 month retainer plan.</p>
                </div>
            </div>      
            
 
            <div class="row">
                
                <div class="large-12 medium-12 small-12 columns text-center">

                    <div class="row collapse pricingwrapper">
                        <div class="large-3 columns pricingcolbg1">
                            <div class="row pricingheader"></div>
                            <div class="row pricingrow"><p>Traffic Management of SEO, PPC or Facebook Ads (Not Including Ad Spend)</p></div>
                            <div class="row pricingrow"><p>Marketing Strategy</p></div>
                            <div class="row pricingrow"><p>Sales Funnel Development</p></div>
                            <div class="row pricingrow"><p>Website Optimization</p></div>                                                     
                        </div>
                        <div class="large-3 columns pricingcolbg2">
                            <div class="row pricingheader">ESSENTIALS<br>$3,500/Month</div>
                            <div class="row pricingrow"><p>1 Traffic Source</p></div>
                            <div class="row pricingrow"><i class="fi-check"></i></div>                            
                            <div class="row pricingrow"><i class="fi-check"></i></div>
                            <div class="row pricingrow"><i class="fi-check"></i></div>                         
                        </div>
                        <div class="large-3 columns pricingcolbg2">
                            <div class="row pricingheader">DELUXE<br>$5,500/Month</div>
                            <div class="row pricingrow"><p>2 Traffic Sources</p></div>
                            <div class="row pricingrow"><i class="fi-check"></i></div>                            
                            <div class="row pricingrow"><i class="fi-check"></i></div>
                            <div class="row pricingrow"><i class="fi-check"></i></div>                        
                        </div>
                        <div class="large-3 columns pricingcolbg2">
                            <div class="row pricingheader">COMPLETE<br>$10,000/Month</div>
                            <div class="row pricingrow"><p>3 Traffic Sources</p></div>
                            <div class="row pricingrow"><i class="fi-check"></i></div>                            
                            <div class="row pricingrow"><i class="fi-check"></i></div>
                            <div class="row pricingrow"><i class="fi-check"></i></div>                        
                        </div>        
                    </div>
                
                </div>
                
            </div>
            
            
             <div class="row">
                <div class="columns text-center">
                    <p><img src="<?php echo $base; ?>img/QuoteLogo.png" /></p>
                </div>
                <div class="columns text-center">
                    <p>“VERY much appreciate the smooth back-end organization and solid project management processes "behind the scenes," well prepared reports and presentations, and well analyzed data. Overall, the experience brought a huge sense of relief and excitement around what's possible to the team and to the executives. Thanks!” <br/>- Aydika James</p>
                </div>
            </div>
            
            <div class="row">
                <div class="large-5 columns large-centered text-center">
                            <input type="submit" class="button radius appsubmitbuttonbig" value="Start My Application Process" />
                </div> 
            </div>            
            
            
    </div>          

</section>



<section>

</section>

<?php include "footer.php";?>